import pickle
from gib import gib_detect_train
from dga_routines import count_consonants, entropy
import tldextract
import argparse
import json
import time
import errno
import os
from datetime import datetime


def read_file(filename):
    with open(filename) as f:
        for line in f:
            yield line.strip("\n")


def domain_check(domain):
    # skip tor domains
    if domain.endswith(".onion"):
        print("Tor domain is ignored...")
        return
    # we only interested in main domain name without subdomain and tld
    domain_without_sub = tldextract.extract(domain).domain
    # skip localized domains
    if domain_without_sub.startswith("xn-"):
        print("Localized domain is ignored...")
        return
    # skip short domains
    if len(domain_without_sub) < 8:
        print("Short domain (<8 chars) is ignored...")
        return
    domain_entropy = entropy(domain_without_sub)
    domain_consonants = count_consonants(domain_without_sub)
    domain_length = len(domain_without_sub)
    return domain_without_sub, domain_entropy, domain_consonants, domain_length


def filecreation(list, filename):
    mydir = os.path.join(
        os.getcwd(), 
        datetime.now().strftime('%Y-%m-%d_%H-%M-%S'))
    try:
        os.makedirs(mydir)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise  # This was not a "directory exist" error..
    with open(os.path.join(mydir, filename), 'w') as d:
        d.writelines(list)


def main():
    os.chdir("/opt/iai/dga_detector-master")
    parser = argparse.ArgumentParser(description="DGA domain detection")
    parser.add_argument("-d", "--domain", help="Domain to check")
    parser.add_argument("-f", "--file", help="File with domains. One per line")
    args = parser.parse_args()
    model_data = pickle.load(open('gib/gib_model.pki', 'rb'))
    model_mat = model_data['mat']
    threshold = model_data['thresh']
    if args.domain:
        if domain_check(args.domain):
            domain_without_sub, domain_entropy, domain_consonants, domain_length = domain_check(args.domain)
            print("Analysing domain...")
            if domain_entropy > 3.8:
                print("High entropy(>3.8) is a strong indicator of DGA domain.\n"
                      "This domain scored: %d" % domain_entropy)
            if domain_consonants > 7:
                print("High consonants(>7) count is an indicator of DGA domain\n"
                      "This domain scored: %d" % domain_consonants)
            if domain_length > 12:
                print("Long domain name(>12) can also indicate DGA\n"
                      "This domain scored: %d" % domain_length)
            if not gib_detect_train.avg_transition_prob(domain_without_sub, model_mat) > threshold:
                print("Domain %s is DGA!" % args.domain)
                print("Soglia",threshold) 
            else:
                print("Domain %s is not DGA! Probably safe :)\n"
                      "Don't quote me on that though\n"
                      "Additional information: \n"
                      "Entropy: %d\n"
                      "Consonants count: %d\n"
                      "Name length: %d" % (args.domain, domain_entropy, domain_consonants, domain_length))
    elif args.file:
        domains = read_file(args.file)
        #The result is the original format to write the analisy as JSON, we kept for memories
        results = {"domain": "", "is_dga": "", "high_consonants": "", "high_entropy": "", "long_domain": "", "end": "", "dtz": ""}
        now = datetime.now()
        timestr = now.strftime('%Y%m%d_%H%M%S.%f')
        #This is to create a folder with the current date where the DGA found files are stored
        mydir = os.path.join(
        os.getcwd(), 
        datetime.now().strftime('%Y-%m-%d'))
        try:
          os.makedirs(mydir)
        except OSError as e:
           if e.errno != errno.EEXIST:
              raise  # This was not a "directory exist" error..
        
        with open(datetime.now().strftime('%Y-%m-%d')+"/"+"dga_found_"+timestr+".txt", "w") as f:
            for domain in domains:
                splitted = domain.split(',')
                CEFprefix = splitted[0] # Here we take the CEF prefix, e.g., CEF:0|DNS_Vendor|DNS_CED|1.0|100|DNS query|5|
                domain = splitted[1] # Here we take only the domain, e.g., google.com
                end = splitted[2] # Here we take only the timestamp, e.g., 1505484703432
                dtz = splitted[3] # Her we take the timezone, e.g., Europe/Berlin
                print("Analysing %s" % domain)
                results["domain"] = domain
                if domain_check(domain):
                    domain_without_sub, domain_entropy, domain_consonants, domain_length = domain_check(domain)
                    if domain_entropy > 3.8:
                        results["high_entropy"] = domain_entropy
                    elif domain_consonants > 7:
                        results["high_consonants"] = domain_consonants
                    elif domain_length > 12:
                        results["long_domain"] = domain_length
                    if not gib_detect_train.avg_transition_prob(domain_without_sub, model_mat) > threshold:
                        results["is_dga"] = True
                        f.write(CEFprefix) #From here we write the CEF file with the DGA found
                        f.write("msg="+domain+" ")
                        f.write("end="+end+" ")
                        f.write("dtz="+dtz+"\n")
                    else:
                        results["is_dga"] = False                                              
                #json.dump(results, f, indent=4)               
        print("File dga_found_"+timestr+".txt created")
    else:
        print('''
_______________________       ________     _____           _____
___  __ \_  ____/__    |      ___  __ \______  /_____________  /______________
__  / / /  / __ __  /| |      __  / / /  _ \  __/  _ \  ___/  __/  __ \_  ___/
_  /_/ // /_/ / _  ___ |      _  /_/ //  __/ /_ /  __/ /__ / /_ / /_/ /  /
/_____/ \____/  /_/  |_|      /_____/ \___/\__/ \___/\___/ \__/ \____//_/
        ''')
        parser.print_help()
main()
