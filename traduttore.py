#!/usr/bin/env python3

import pickle

with open("gib/gib_model.pki", "rb") as f:
    w = pickle.load(f)

pickle.dump(w, open("gib/gib_model.pki","wb"), protocol=2)